Feature: validate search with search Type "EPP_Search by_Billing Account"

@EPP_Searchby_BillingAccount
Scenario: Validating search functionality 
Given I am in EPP url with "EPP_Search by_Billing Account"
When I login to the application
Then I click to the search button
Then I select search type
Then  I logout