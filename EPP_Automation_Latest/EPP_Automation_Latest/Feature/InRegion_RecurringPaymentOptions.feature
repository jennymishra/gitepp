Feature: validate login with Transaction Type "InRegion_RecurringPaymentOptions"


@InRegion_RecurringPaymentOptions
Scenario: Validating login functionality with CRIS and ACH
Given I am in EPP url with "InRegion_RecurringPaymentOptions"
When I login to the application
Then I enter accountNumber
Then I select Transaction type 
And I fill all the mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout
