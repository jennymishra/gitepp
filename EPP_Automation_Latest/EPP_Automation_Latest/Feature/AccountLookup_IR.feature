Feature: validate login with Transaction Type "AccountLookup_IR"

@AccountLookup_IR
Scenario: Validating account lookup functionality with CRIS
Given I am in EPP url with "AccountLookup_IR"
When I login to the application
Then I enter accountNumber
Then I select Transaction type
Then I validate whether account lookup is successful
Then  I logout