package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.SearchPage;

import cucumber.api.java.en.Then;

public class SearchPageStepDefinition extends ExcelOperations {

	ExcelOperations opr =  new ExcelOperations();

	@Then("^I click to the search button$")
	public void i_click_to_the_search_button() throws Throwable {
		SearchPage.clickOnSearchButton();
	}

	@Then("^I select search type$")
	public void i_select_search_type() throws Throwable {
		SearchPage.selectSearchType();
	}
	
	@Then("^I verify PaymentID$")
	public void i_verify_PaymentID() throws Throwable {
		SearchPage.DBVerification();
	}
	
	@Then("^I click on the payment_id$")
	public void i_click_on_payment_id$() throws Throwable {
		SearchPage.clickOnPayment_Id();
	}
}
