package com.EPP_Automation.BusinessSpecific;

import java.sql.ResultSet;

import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.EPP_HomePageFactory;
import com.EPP_Automation.PageFactory.EPP_SearchPageFactory;

public class SearchPage extends CommonSeleniumClass{

	public static void clickOnSearchButton()
	{
		click(EPP_HomePageFactory.clickOnSearchButton);
	}
	
	public static void clickOnPayment_Id(){
		click(EPP_SearchPageFactory.PaymentID);
	}
	
	public static void selectSearchType()
	{
		selectOptionByVisibleText(EPP_SearchPageFactory.SelectSearchType,GlobalVariables.Ex_SearchType);
		switch (GlobalVariables.Ex_SearchType) {

		case "Customer Last Name":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		case "Order Number":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		case "Confirmation Number":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		case "Billing Account":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Serial Number":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Business Name":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Payment Verification Id":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "ePay Payment Confirmation":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Recurring Wallet":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		default :

			System.out.println("PLEASE SELECT SEARCH TYPE");
		}

	}

	//DB validation for Searched PaymentID	
	public static void DBVerification() throws Exception{
		String PaymentID = EPP_SearchPageFactory.PaymentID.getText();
		System.out.println("PaymentID = "+PaymentID);
		String result= null;
		String Query = "Select PAYMENT_STATUS_CD from payment where payment_id in "+PaymentID;
		ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","epwf_app", "epwf_app_epwfst1", Query);
		while (rset.next()) {
			result =rset.getString("PAYMENT_STATUS_CD");
		}
		System.out.println("Status of PaymentID = "+result);
		
		System.out.println("PASSED");
	}
	/*public static void selectSearchvalue()
	{
		enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);     
}*/}
