package com.EPP_Automation.BusinessSpecific;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.junit.Assert;

import com.EPP_Automation.Common.CommonClass;
import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;
import com.EPP_Automation.PageFactory.EPP_TransactionInformationPageFactory;

public class NegotiatePaymentPage extends CommonSeleniumClass
{

	public static void NegotiatePaymentFields()
	{  
		switchToFrameByElement(EPP_NegotiatePaymentPageFactory.switchToIframe);

		selectOptionByVisibleText(EPP_NegotiatePaymentPageFactory.methodOfPayment,GlobalVariables.Ex_MethodOfPayment);



		switch (GlobalVariables.Ex_MethodOfPayment) {

		case "Credit or Debit Account":

			enter(EPP_NegotiatePaymentPageFactory.cardNumber,GlobalVariables.Ex_CardNumber);
			selectOptionByVisibleText(EPP_NegotiatePaymentPageFactory.expiryMonth,GlobalVariables.Ex_ExpiryMonth);
			selectOptionByVisibleText(EPP_NegotiatePaymentPageFactory.expiryYear,GlobalVariables.Ex_ExpiryYear);
			clear(EPP_NegotiatePaymentPageFactory.zipCode);
			enter(EPP_NegotiatePaymentPageFactory.zipCode,GlobalVariables.Ex_Zipcode);
			click(EPP_NegotiatePaymentPageFactory.checkBox);
			click(EPP_NegotiatePaymentPageFactory.nextButtton);
			switch(GlobalVariables.Ex_PaymentType){
			
			case "MultiPayment":
				click(EPP_NegotiatePaymentPageFactory.submitPayment);
				break;
			
	    	case "SinglePayment":
	    		enter(EPP_NegotiatePaymentPageFactory.emailBox,GlobalVariables.Ex_EmailId);
	    		click(EPP_NegotiatePaymentPageFactory.submitPayment);
	    		break;
	    	default:
	    		System.out.println();
	    		
			}
		    		
			

			//String email = getAttribute(EPP_NegotiatePaymentPageFactory.emailBox, "value");

			//if(email !=null)
			/*try{
			if(getAttribute(EPP_NegotiatePaymentPageFactory.emailBox, "value").equalsIgnoreCase(""))
			{
				clear(EPP_NegotiatePaymentPageFactory.emailBox);
				enter(EPP_NegotiatePaymentPageFactory.emailBox,GlobalVariables.Ex_EmailId);
				click(EPP_NegotiatePaymentPageFactory.submitPayment);
			}
			}
			catch(Exception e)
			{
				click(EPP_NegotiatePaymentPageFactory.submitPayment);
			}*/


			break;

		default :

			enter(EPP_NegotiatePaymentPageFactory.routingNumber,GlobalVariables.Ex_RoutingNumber);
			enter(EPP_NegotiatePaymentPageFactory.accountNumber,GlobalVariables.Ex_AccountNumber);
			click(EPP_NegotiatePaymentPageFactory.checkBox);
			click(EPP_NegotiatePaymentPageFactory.nextButtton);
			switch(GlobalVariables.Ex_PaymentType){
			
				case "MultiPayment":
					click(EPP_NegotiatePaymentPageFactory.submitPayment);
					break;
			
				case "SinglePayment":
					enter(EPP_NegotiatePaymentPageFactory.emailBox,GlobalVariables.Ex_EmailId);
	    			click(EPP_NegotiatePaymentPageFactory.submitPayment);
	    			break;
				default:
					System.out.println();
	    		
			}
//			String email1 = getAttribute(EPP_NegotiatePaymentPageFactory.emailBox, "value");
//
/*//			if(email1 !=null)
			try{
				if(getAttribute(EPP_NegotiatePaymentPageFactory.emailBox, "value").equalsIgnoreCase(""))
				{
					clear(EPP_NegotiatePaymentPageFactory.emailBox);
					enter(EPP_NegotiatePaymentPageFactory.emailBox,GlobalVariables.Ex_EmailId);
					click(EPP_NegotiatePaymentPageFactory.submitPayment);
				}
				}
				catch(Exception e)
				{
					click(EPP_NegotiatePaymentPageFactory.submitPayment);
				}



		}*/
		}
		
	

	}
	
    //DB validation for single Payment
	public static void DBVerification() throws Exception{
		String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumber.getText();
		System.out.println("confirmationNumber ="+" "+confirmationNumber);
		String result= null;
		String Query = "Select PAYMENT_STATUS_CD from payment where payment_id in"+"('"+confirmationNumber+"')";
		//System.out.println("Query ="+Query);
		//DatabaseConn.run(Query);
		ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query);
		//System.out.println("rset = "+rset);
		while (rset.next()) {
			result =rset.getString("PAYMENT_STATUS_CD");
		}
		/*if(result.equals("Settlement_Completed")) 
		{
			System.out.println("verification successful");
		}

		else if(result.equals("Capture_Ready"))
		{
			System.out.println("verification successful");
		}
		else{
			System.out.println("verification successful");
		} */
		System.out.println(result);
	}


	//DB validation for Multipayment Payment
	public static void DBVerificationForMultiPayment() throws Exception{
		String result=null;
		String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumberForMultiPayment.getText();
		System.out.println("PaymentVerificationReferenceValue="+confirmationNumber);
		String Query1 = "Select payment_id from payment where pmt_verification_reference_Val in "+"('"+confirmationNumber+"')";
		ResultSet rset1 = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query1);
		
		List<String> paymentId= new ArrayList<String>();
		List<String> paymentStatusCd= new ArrayList<String>();
		
		while (rset1.next()) {
			paymentId.add(rset1.getString("PAYMENT_ID"));
		}
		
		for(int i=0;i<paymentId.size();i++){
			String Query2 = "Select payment_status_cd from payment where payment_id in "+"('"+paymentId.get(i)+"')";
			ResultSet rset2 = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP", "epwf_app_epwfst1", Query2);
			while (rset2.next()) {
				paymentStatusCd.add(rset2.getString("PAYMENT_STATUS_CD"));
			}
		}
		
		for(int j=0;j<paymentId.size();j++){
			for(int k=j;k<=j;k++){
				if(k>0){
					result+=paymentId.get(j)+"("+paymentStatusCd.get(k)+")"+" ";
				}else{
					result=paymentId.get(j)+"("+paymentStatusCd.get(k)+")"+" ";
				}
			}	
		}
		System.out.println(result);
		//exo.setReportForTransaction(result,GlobalVariables.Re_SheetName,GlobalVariables.Re_PaymentResult);
	}

}







