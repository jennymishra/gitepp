package com.EPP_Automation.BusinessSpecific;

import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.PageFactory.EPP_LoginLogoutPageFactory;


public class LoginLogoutPage extends CommonSeleniumClass{

	public static void goToEPPLoginPage(){

		CommonSeleniumClass.initiatePageFactory();
		selectRequiredBrowser(GlobalVariables.Ex_BrowserToUse);
		navigate(GlobalVariables.Ex_EPPURL);
		maximizeWindow();

	}

	public static void loginCredentials(){
		CommonSeleniumClass.initiatePageFactory();
		enter(EPP_LoginLogoutPageFactory.userName,GlobalVariables.Ex_Username);
		enter(EPP_LoginLogoutPageFactory.password,GlobalVariables.Ex_Password);
		click(EPP_LoginLogoutPageFactory.clickOnLogIn);
	}


	public static void logout(){
		CommonSeleniumClass.initiatePageFactory();
		think(1);
		//click(EPP_LoginLogoutPageFactory.clickOnLogOut);
		closeAllWindows();
	}
}