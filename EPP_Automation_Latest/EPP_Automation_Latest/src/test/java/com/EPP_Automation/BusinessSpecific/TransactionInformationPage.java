package com.EPP_Automation.BusinessSpecific;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.CalendarPageFactory;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;
import com.EPP_Automation.PageFactory.EPP_TransactionInformationPageFactory;

import cucumber.api.java.en.Then;

public class TransactionInformationPage extends CommonSeleniumClass {
	
	
	
	public static void fillFields() throws ClassNotFoundException, SQLException{

		//String actualCityData = getAttribute(EPP_TransactionInformationPageFactory.city, "value");

		//String actualBussinessName = getAttribute(EPP_TransactionInformationPageFactory.enterBussinessName, "value");

/*		try{
			if(actualCityData.equalsIgnoreCase(""))
			{	
				enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
				enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);
				enter(EPP_TransactionInformationPageFactory.address,GlobalVariables.Ex_Address);
				enter(EPP_TransactionInformationPageFactory.city,GlobalVariables.Ex_City);
				enter(EPP_TransactionInformationPageFactory.state,GlobalVariables.Ex_State);
				enter(EPP_TransactionInformationPageFactory.zipcode,GlobalVariables.Ex_ZipCode);
			click(EPP_TransactionInformationPageFactory.calendarButton);
				String Query = "select sysdate from dual" ;
				
				//clear(EPP_TransactionInformationPageFactory.enterPaymentDate);
				//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
				//clear(EPP_TransactionInformationPage.enterPaymentAmount);
				//enter(EPP_TransactionInformationPage.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
				selectOptionByVisibleText(EPP_TransactionInformationPageFactory.selectEntityCode,GlobalVariables.Ex_EntityCode);
				clear(EPP_TransactionInformationPageFactory.enterEntityInfo);
				enter(EPP_TransactionInformationPageFactory.enterEntityInfo,GlobalVariables.Ex_EntityInfoAmount);
				//click(EPP_TransactionInformationPage.checkConvenienceFeeConfirmation);
			click(EPP_TransactionInformationPageFactory.addPayment);
		}*/
		
		switch(GlobalVariables.Ex_PaymentType){
		
		case "MultiPayment":
			enter(EPP_TransactionInformationPageFactory.enterBussinessName,GlobalVariables.Ex_BusinessName);
			enter(EPP_TransactionInformationPageFactory.enterAditionalBAN,GlobalVariables.Ex_AdditionalBAN);
			//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
			clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
			enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
			click(EPP_TransactionInformationPageFactory.addAditionalBAN);
			clear(EPP_TransactionInformationPageFactory.aditionalAmount);
			enter(EPP_TransactionInformationPageFactory.aditionalAmount,GlobalVariables.Ex_AditionalAmount);
			click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
			click(EPP_TransactionInformationPageFactory.addPayment);
			break;
		
		case "SinglePayment":
			enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
			enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);
			//clear(EPP_TransactionInformationPageFactory.enterPaymentDate);
			//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
			if(GlobalVariables.Ex_ScheduleType.equals("Scheduled")){
				click(EPP_TransactionInformationPageFactory.calendarButton);
				String Query = "select sysdate from dual" ;
				ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db.dev.qintra.com:1602:epwfst1","EPWF_APP", "epwf_app_epwfst1", Query);
				String date = null;
				while (rset.next()) {
					date =rset.getString("SYSDATE").substring(8,10);
				}
				int dateInt=Integer.parseInt(date);
				String parentWindow = driver.getWindowHandle();
				Set<String> handles =  driver.getWindowHandles();
				for(String windowHandle  : handles)
			       {
			       if(!windowHandle.equals(parentWindow))
			          {
			    	   	driver.switchTo().window(windowHandle);
			    	   	try{
			    	   		click(CalendarPageFactory.getDate(dateInt+1, driver));
			    	   	}catch(Exception e){
			    	   		
			    	   	}
			    	   	driver.switchTo().window(parentWindow);
			          }
			       }
			}
			clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
			enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
			click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
			click(EPP_TransactionInformationPageFactory.addPayment);
			break;
		
		default:
			System.out.println();
		}
			
		/*try{
			enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
			enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);
			//clear(EPP_TransactionInformationPageFactory.enterPaymentDate);
			//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
			if(GlobalVariables.Ex_ScheduleType.equals("Scheduled")){
				click(EPP_TransactionInformationPageFactory.calendarButton);
				String Query = "select sysdate from dual" ;
				ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db.dev.qintra.com:1602:epwfst1","EPWF_APP", "epwf_app_epwfst1", Query);
				String date = null;
				while (rset.next()) {
					date =rset.getString("SYSDATE").substring(8,10);
				}
				int dateInt=Integer.parseInt(date);
				String parentWindow = driver.getWindowHandle();
				Set<String> handles =  driver.getWindowHandles();
				for(String windowHandle  : handles)
			       {
			       if(!windowHandle.equals(parentWindow))
			          {
			    	   	driver.switchTo().window(windowHandle);
			    	   	click(CalendarPageFactory.getDate(dateInt+1, driver));
			    	   	driver.switchTo().window(parentWindow);
			          }
			       }
			}
			clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
			enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
			click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
			click(EPP_TransactionInformationPageFactory.addPayment);

		}
		catch(Exception e) 
		{
			//if(getAttribute(EPP_TransactionInformationPageFactory.enterBussinessName, "value").equalsIgnoreCase(""))
			//if(EPP_TransactionInformationPageFactory.addAditionalBAN.isDisplayed())
			//{
				enter(EPP_TransactionInformationPageFactory.enterBussinessName,GlobalVariables.Ex_BusinessName);
				enter(EPP_TransactionInformationPageFactory.enterAditionalBAN,GlobalVariables.Ex_AdditionalBAN);
				//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
				clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
				enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
				click(EPP_TransactionInformationPageFactory.addAditionalBAN);
				clear(EPP_TransactionInformationPageFactory.aditionalAmount);
				enter(EPP_TransactionInformationPageFactory.aditionalAmount,GlobalVariables.Ex_AditionalAmount);
				click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
				click(EPP_TransactionInformationPageFactory.addPayment);
			//}

		}*/
	}
	
	
	public static void fillImpFields()
	{
		if(getText(EPP_TransactionInformationPageFactory.PaymentMethod).equalsIgnoreCase("not enrolled in autopay"))
				{
			       click(EPP_TransactionInformationPageFactory.AutomaticPaymentEnrollment);
			       click(EPP_TransactionInformationPageFactory.UpdateBillingInfo);
			       switchToFrameByElement(EPP_TransactionInformationPageFactory.NegotiateChangeFrame);
			       click(EPP_TransactionInformationPageFactory.AddNewDebitCreditCard);
			       enter(EPP_TransactionInformationPageFactory.AccountNickName, GlobalVariables.Ex_CustomerFirstName);
			       enter(EPP_TransactionInformationPageFactory.CardNumber, GlobalVariables.Ex_CardNumber);
			       selectOptionByVisibleText(EPP_TransactionInformationPageFactory.CardExpMonth,GlobalVariables.Ex_ExpiryMonth);//element are not inspecting
				   selectOptionByVisibleText(EPP_TransactionInformationPageFactory.CardExpYear,GlobalVariables.Ex_ExpiryYear);//element are not inspecting
			       enter(EPP_TransactionInformationPageFactory.CardBillingZip,GlobalVariables.Ex_Zipcode);
			       click(EPP_TransactionInformationPageFactory.FinishButton);
				}
		else
		{
			click(EPP_TransactionInformationPageFactory.AutomaticPaymentUnEnrollment);
		}
	}
	
	public static void validateAccountLookupSuccess(){
		if(EPP_TransactionInformationPageFactory.addNewPaymentText.isDisplayed()){
			System.out.println("AccountLookupSuccessful");
		}
	}
}


