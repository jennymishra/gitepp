package com.EPP_Automation.BusinessSpecific;

@SuppressWarnings("serial")
public class AutomationFrameworkException extends Exception{

	public AutomationFrameworkException(String errormessage) {
		super(errormessage);
	}

}
