package com.EPP_Automation.BusinessSpecific;

import java.io.File;

import com.EPP_Automation.Common.ExcelUtils;

public class ExcelOperations extends ExcelUtils{
	private int colnum = 0;
	ExcelUtils excelOperations = new ExcelUtils();

	private GlobalVariables globalVariables = new GlobalVariables();

	public void setVariablesFromExcelRCSheet(String sheetName, File excelFile, int rowNum) throws Exception {

		excelOperations.setExcelFile(globalVariables.ExcelFilePath,
				globalVariables.RC_SheetName);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_BrowserToUse);
		globalVariables.Ex_BrowserToUse = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_BusinessName);
		globalVariables.Ex_BusinessName = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_CustomerFirstName);
		globalVariables.Ex_CustomerFirstName = excelOperations
				.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_CustomerLastName);
		globalVariables.Ex_CustomerLastName = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_EPPURL);
		globalVariables.Ex_EPPURL = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_Password);
		globalVariables.Ex_Password = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_Username);
		globalVariables.Ex_Username = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_BTN);
		globalVariables.Ex_BTN = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_TransactionType);
		globalVariables.Ex_TransactionType = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_PaymentAmount);
		globalVariables.Ex_PaymentAmount = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_OrderNumber);
		globalVariables.Ex_OrderNumber = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_AccountSuffix);
		globalVariables.Ex_AccountSuffix = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_MarketUnit);
		globalVariables.Ex_MarketUnit = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_PaymentDate);
		globalVariables.Ex_PaymentDate = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_Address);
		globalVariables.Ex_Address = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_State);
		globalVariables.Ex_State = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_City);
		globalVariables.Ex_City = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_ZipCode);
		globalVariables.Ex_ZipCode = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_EntityInfoAmount);
		globalVariables.Ex_EntityInfoAmount = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_AdditionalBAN);
		globalVariables.Ex_AdditionalBAN = excelOperations.getCellData(rowNum, colnum);
		
		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_EntityCode);
		globalVariables.Ex_EntityCode = excelOperations.getCellData(rowNum, colnum);
		
		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_AditionalAmount);
		globalVariables.Ex_AditionalAmount = excelOperations.getCellData(rowNum, colnum);
	
		excelOperations.setExcelFile(globalVariables.ExcelFilePath,
				globalVariables.RC_SheetName);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_MethodOfPayment);
		globalVariables.Ex_MethodOfPayment = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_CardNumber);
		globalVariables.Ex_CardNumber = excelOperations.getCellData(rowNum, colnum);


		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_ExpiryMonth);
		globalVariables.Ex_ExpiryMonth = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_ExpiryYear);
		globalVariables.Ex_ExpiryYear = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_Zipcode);
		globalVariables.Ex_Zipcode = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_EmailId);
		globalVariables.Ex_EmailId = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_RoutingNumber);
		globalVariables.Ex_RoutingNumber = excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.IF_AccountNumber);
		globalVariables.Ex_AccountNumber= excelOperations.getCellData(rowNum, colnum);
		
		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_SearchType);
		globalVariables.Ex_SearchType= excelOperations.getCellData(rowNum, colnum);

		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_SearchValue);
		globalVariables.Ex_SearchValue= excelOperations.getCellData(rowNum, colnum);
		
		colnum = excelOperations.getColumnNo(globalVariables.RC_SheetName,
				globalVariables.RC_ToDate);
		globalVariables.Ex_ToDate= excelOperations.getCellData(rowNum, colnum);
		
		colnum = excelOperations.getColumnNo(GlobalVariables.RC_SheetName,
				globalVariables.RC_ScheduleType);
		globalVariables.Ex_ScheduleType= excelOperations.getCellData(rowNum, colnum);
		
		colnum = excelOperations.getColumnNo(GlobalVariables.RC_SheetName,
				globalVariables.RC_PaymentType);
		globalVariables.Ex_PaymentType= excelOperations.getCellData(rowNum, colnum);


	}

}
