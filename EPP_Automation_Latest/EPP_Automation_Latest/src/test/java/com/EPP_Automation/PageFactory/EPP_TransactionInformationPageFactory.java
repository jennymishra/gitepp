package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EPP_TransactionInformationPageFactory {
	
	@FindBy(xpath="(//td[contains(text(),'Add New')])[1]")
	public static WebElement addNewPaymentText;
	
	@FindBy(id="id_PayerName")
	public static WebElement enterFirstName;

	@FindBy(id="id_PayerLastName")
	public static WebElement enterLastName;

	@FindBy(name="custConvChk")
	public static WebElement checkConvenienceFeeConfirmation;

	@FindBy(name="addBtn")
	public static WebElement addPayment;

	@FindBy(id="id_Pmt")
	public static WebElement enterPaymentAmount;

	@FindBy(xpath="//input[@id='id_PayDate']")
	public static WebElement enterPaymentDate;

	@FindBy(id="id_Addr1")
	public static WebElement address;

	@FindBy(id="id_City")
	public static WebElement city;

	@FindBy(id="id_State")
	public static WebElement state;

	@FindBy(id="id_Zip")
	public static WebElement zipcode;

	@FindBy(id="id_EntityAmount0")
	public static WebElement enterEntityInfo;

	@FindBy(id="id_EntityName0")//need to enter xpath
	public static WebElement selectEntityCode;

	//for MultiPayment

	@FindBy(xpath="//table[@id='tbl']//tr//td//input[@id='id_PayerBizName']")
	public static WebElement enterBussinessName;

	@FindBy(xpath="//input[@id='id_AddBtn']")
	public static WebElement enterAditionalBAN;
	
	@FindBy(id="id_BtnAddBTN")
	public static WebElement addAditionalBAN;
	
	@FindBy(id="amtTf1")
	public static WebElement aditionalAmount;

 //for Recurring Payment Options
	
	@FindBy(xpath="//td[@class='CONTENT']")
	public static WebElement PaymentMethod ;
	
	@FindBy(xpath="//input[@value='auto']")
	public static WebElement AutomaticPaymentEnrollment;
	
	@FindBy(xpath="//input[@value='noAuto']")
	public static WebElement AutomaticPaymentUnEnrollment;
	
	@FindBy(name="updBtn")
	public static WebElement UpdateBillingInfo; 

	@FindBy(id="payFrame")
	public static WebElement NegotiateChangeFrame;
	
	@FindBy(id="PageContent_ADD_CC_LINK")
	public static WebElement AddNewDebitCreditCard;
	
	@FindBy(name="NICKNAME")
	public static WebElement AccountNickName;
	
	@FindBy(name="DEBIT_ACCOUNT")  
	public static WebElement CardNumber;

	@FindBy(name="Next")
	public static WebElement NextButton;
	
	@FindBy(name="CARD_EXPIRATION_MONTH")
	public static WebElement CardExpMonth;
	
	@FindBy(name="CARD_EXPIRATION_YEAR")
	public static WebElement CardExpYear;
	
	@FindBy(name="DEBIT_ZIP")
	public static WebElement CardBillingZip; 
	
	@FindBy(name="Submit9")
	public static WebElement FinishButton;
	
	//For scheduled payment
	@FindBy(xpath="//img[contains(@title,'Calendar')]")
	public static WebElement calendarButton;
	
	
}
