package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EPP_SearchPageFactory {

	
	@FindBy(id="id_SearchType")
	public static WebElement SelectSearchType; 
	
	@FindBy(id="id_SearchValue")
	public static WebElement EnterSearchValue; 
	@FindBy(id="id_FromDate")
	public static WebElement FromDate;
	
	@FindBy(id="id_FromDate")
	public static WebElement ToDate;
	
	@FindBy(xpath="//input[@type='SUBMIT']")
	public static WebElement clickOnSearchNowButton;
	
	@FindBy(xpath="//table[@id='srchTbl']//tbody//tr[2]//td[1]//a")
	public static WebElement PaymentID;
	
}
