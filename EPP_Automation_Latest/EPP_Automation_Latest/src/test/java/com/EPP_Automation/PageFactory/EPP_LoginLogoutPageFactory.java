package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EPP_LoginLogoutPageFactory {
	
	@FindBy(xpath="//input[@name='user']")
	public static WebElement userName;
	
	@FindBy(xpath="//td[contains(text(),'Password')]/following-sibling::td[2]/input")
	public static WebElement password;
	
	@FindBy(xpath="//input[@value='Log In']")
	public static WebElement clickOnLogIn;
	
	@FindBy(xpath="(//a[text()='Logout'])[1]")
	public static WebElement clickOnLogOut;
}
