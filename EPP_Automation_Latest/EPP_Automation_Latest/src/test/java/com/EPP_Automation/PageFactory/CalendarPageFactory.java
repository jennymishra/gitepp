package com.EPP_Automation.PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CalendarPageFactory {
	
	public static WebElement getDate(int date, WebDriver driver){
		
		return driver.findElement(By.xpath("//head[title[text()='Calendar']]/following-sibling::body/table[2]/tbody/tr/td/font/a[text()='"+date+"']"));
		
	}
	
}
