package com.EPP_Automation.Common;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.GlobalVariables;



public class CommonClass {
	ExcelUtils excelUtils = new ExcelUtils();
	
	// Get the current system datetime
	public String getCurrentDateTime(String requiredDateFormat) {
		DateFormat dateFormat = new SimpleDateFormat(requiredDateFormat);
		Calendar cal = Calendar.getInstance();
		String CurrDateTime = dateFormat.format(cal.getTime());
		return CurrDateTime;
	}
		
	
	//JDBC Connectivity
	/*public static ResultSet queryResultFromDB(String query,String dbConnectionStringValue,String dbUserNameValue,String dbPasswordValue) throws SQLException{
		Connection conn =DriverManager.getConnection(dbConnectionStringValue, dbUserNameValue, dbPasswordValue);
		PreparedStatement Prepstmt = conn.prepareStatement(query);
		ResultSet queryResult = Prepstmt.executeQuery();
		return queryResult;
	}*/
	
	 public static ResultSet run()
	 {
		// Open a connection
		 
	      try {
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@epwfst1db.dev.qintra.com:1602:epwfst1", "EPWF_APP", "epwf_app_epwfst1");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	      
	 }
	
	//Create a Random number with given length
	public String getRandomNumber(int length) {
		Random rnd = new Random();
	    long nextLong = Math.abs(rnd.nextLong());
	    return String.valueOf(nextLong).substring(0, length);
	}
	
	//Add List into String
	public String addListIntoString(ArrayList<String> list){
		String stringToBeReturned = "";
		for(String itr : list){
			stringToBeReturned += itr +"\n";
		}
		
		return stringToBeReturned;
		
	}


	// Take the assert values from Excel and split them
	public List<String> addAssertValueToArrayList(String sheetName, int rowNumber,String AssertName)
			throws Exception {
		int colNumber;
		String assertUnsplittedString;
		colNumber = excelUtils.getColumnNo(sheetName,AssertName);
		assertUnsplittedString = excelUtils.getCellData(rowNumber, colNumber);
		List<String> assertValues = new ArrayList<String>();
		assertValues = Arrays.asList(assertUnsplittedString
				.split(","));
		return assertValues;
	}
	
	// Common methods for feature file
	public static void data(String data) throws Exception{
		ExcelOperations opr =  new ExcelOperations();
		ExcelUtils excelUtils = new ExcelUtils();
		File testDataExcelFile = GlobalVariables.ExcelFilePath;
		if(data.equalsIgnoreCase("InRegion_EPP_SubmitPayment_ACH_CRIS")){
		int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
		opr.setVariablesFromExcelRCSheet(
				GlobalVariables.RC_SheetName, testDataExcelFile,
				row);
		}else if(data.equalsIgnoreCase("InRegion_EPP_SubmitPayment_CARD_CRIS")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("OutOfRegion_EPP_SubmitPayment_ACH_LATIS")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("OutOfRegion_EPP_SubmitPayment_CARD_LATIS")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("EPP_Multipayment_ACH")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("EPP_Multipayment_CARD")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("InRegion_RecurringPaymentOptions")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("Search")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("Refund")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("AccountLookup_IR")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("AccountLookup_OOR")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("EPP_Search by_Billing Account")) 
{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
}
	}

}
