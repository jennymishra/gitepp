package com.EPP_Automation.Common;
import  java.sql.Connection;
import  java.sql.DriverManager;
import  java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import  java.sql.Statement;
import java.util.Vector;

public class DatabaseConn 
{


	public static ResultSet  OpenDBConnection(String url, String username, String password, String Query) throws SQLException, ClassNotFoundException{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection =DriverManager.getConnection(url,username,password);
		Statement Prepstmt = connection.createStatement();
		ResultSet queryResult = Prepstmt.executeQuery(Query);
		return queryResult;
	}
}

